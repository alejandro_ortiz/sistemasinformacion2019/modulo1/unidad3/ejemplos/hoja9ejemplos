﻿DROP DATABASE IF EXISTS ejemplo9;
CREATE DATABASE ejemplo9;
USE ejemplo9;

-- 1. Procedimiento almacenado para crear la tabla empleados (me permite introducir 
-- datos en la tabla a través de un argumento booleano denominado test)

  DELIMITER //
  CREATE OR REPLACE PROCEDURE tablaEmpleados(test boolean)
    BEGIN
      CREATE OR REPLACE TABLE empleados(
        id int AUTO_INCREMENT,
        nombre varchar(50),
        fecha_nacimiento date,
        edad int,
        correo varchar (50),
        PRIMARY KEY (id)
      )ENGINE MYISAM;

      IF (test=1) THEN
        INSERT INTO empleados (nombre, fecha_nacimiento)
          VALUES 
          ('Juan', '1987/05/12'),
          ('Sara', '1975/01/22'),
          ('Pedro', '1998/02/03');
      END IF;
    END //
  DELIMITER ;

  CALL tablaEmpleados(TRUE);
  SELECT * FROM empleados e;
  CALL tablaEmpleados(FALSE);
  SELECT * FROM empleados e;

-- 2. Procedimiento almacenado para crear la tabla empresa (me permite introducir 
-- datos en la tabla a través de un argumento booleano denominado test)

  DELIMITER //
  CREATE OR REPLACE PROCEDURE tablaEmpresa(test boolean)
    BEGIN
      CREATE OR REPLACE TABLE empresa(
        id int AUTO_INCREMENT,
        nombre varchar(50),
        direccion varchar(50),
        numero_empleados int,
        PRIMARY KEY (id)
      )ENGINE MYISAM;

      IF (test=1) THEN
        INSERT INTO empresa (nombre, direccion)
          VALUES 
            ('empresa1', 'direccion1'),
            ('empresa2', 'direccion2'),
            ('empresa3', 'direccion3');
      END IF;
    END //
  DELIMITER ;

  CALL tablaEmpresa(TRUE);
  SELECT * FROM empresa e;
  CALL tablaEmpresa(FALSE);
  SELECT * FROM empresa e;

-- 3. Realizar un procedimiento almacenado que me actualice la tabla empleados 
-- calculando los campos edad Y correo.
-- a. El campo edad en función de la fecha de nacimiento
-- b. El campo correo utilizando nombre@prueba.es

  DELIMITER //
  CREATE OR REPLACE PROCEDURE actualizaEmpleados()
    BEGIN
      UPDATE empleados e
        set e.edad = TIMESTAMPDIFF(year,e.fecha_nacimiento,NOW()),
            e.correo = CONCAT(e.nombre,'@prueba.es');
    END //
  DELIMITER ;

  CALL actualizaEmpleados();
  SELECT * FROM empleados e;

-- 4. Utilizando disparadores conseguir que en la tabla empleados cuando introduzcamos 
-- o modifiquemos un registro nos calcule la edad y el correo.

  DELIMITER //
  CREATE OR REPLACE TRIGGER empleadosBI
    BEFORE INSERT
    ON empleados
    FOR EACH ROW
    BEGIN
      SET NEW.edad = TIMESTAMPDIFF(year,NEW.fecha_nacimiento,NOW()),
          NEW.correo = CONCAT(NEW.nombre,'@prueba.es');
    END //
  DELIMITER ;

  INSERT INTO empleados (nombre, fecha_nacimiento)
    VALUES ('Alex', '1990/09/05');
  SELECT * FROM empleados e;

-- 5. Procedimiento almacenado para crear la tabla trabajan (me permite introducir 
-- datos en la tabla a través de un argumento booleano denominado test)

  DELIMITER //
  CREATE OR REPLACE PROCEDURE tablaTrabajan(test boolean)
    BEGIN
      CREATE OR REPLACE TABLE trabajan(
        empleado int,
        empresa int,
        fecha_inicio date,
        fecha_fin date,
        baja boolean,
        PRIMARY KEY (empleado, empresa)
      )ENGINE MYISAM;

      IF (test=1) THEN
        INSERT INTO trabajan (empleado, empresa, fecha_inicio, fecha_fin)
          VALUES 
            (1, 3, '2015/12/01', NULL),
            (4, 2, '2000/08/05', '2004/01/01'),
            (2, 1, '2018/07/12', NULL),
            (3, 3, '2012/01/25', '2015/05/14');
      END IF;
    END //
  DELIMITER ;

  CALL tablaTrabajan(TRUE);
  SELECT * FROM trabajan t;
  CALL tablaTrabajan(FALSE);
  SELECT * FROM trabajan t;

-- 6. Disparador para calcular el campo baja de la tabla tabajan al insertar un dato

  DELIMITER //
  CREATE OR REPLACE TRIGGER trabajanBI
    BEFORE INSERT
    ON trabajan
    FOR EACH ROW
    BEGIN
      IF (NEW.fecha_fin=NULL) THEN
        SET NEW.baja = FALSE;
      ELSE
        SET NEW.baja = TRUE;
      END IF;
    END //
  DELIMITER ;

-- 7. Disparador para actualizar el campo baja de la tabla tabajan